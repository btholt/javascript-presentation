# JavaScript
## Why it's like that kid who sat in back of class eating glue

---

## JS: A brief history
- JavaScript was developed in 10 days by an engineer at Netscape
- Despite its name, it has little to do with Java. The name was a marketing ploy by Netscape.
- Because of its hastily developed genesis, JavaScript has some really, really bad parts.
- Due to various trademark conflicts, JS has had the names Mocha, LiveScript, JavaScript, ECMAScript, and EcmaScript.
- Today the name is officially ECMAScript because Oracle owns the name JavaScript.

---

##JS: Future

- Ecma (Eck-ma) International governs the language today after a long history of competing standards. Ecma used to be the European Computer Manufactures Association, but changed that name too due to copyright issues.
- Today we are using ECMAScript 5 (ES5.) The ES6 standard will be finalized in December and tested until December of 2014. At that point, ES6 will become the current standard.
- ES6 will make your life better and could possibly solve world hunger.
- ES6 is also called by JS.next, JS6 and JS Harmony. They all refer to the same thing.
- There was no ES4. We went from ES3 to ES5. Must be a Java thing.

---

# Why JS Eats Glue
## The shitty parts of JS

---

## Scope

- JS _does not_ have block scope.
- JS does have function scope, but it can act in strange ways.
- JS does not have visibility modifiers for its objects (no public/private/protected.)
- JS defaults to putting everything in the global scope: a really, really bad idea.
- ES6 alleviates some of this with "let" declarations instead of "var" declarations.

---

## Scope Demo
- scope.js

---

## Hoisting

- JS does a crazy little thing called hoisting. It take all your variable declarations and pulls them to the top of that scope, usually the top of the current function (or to the global.)
- That was done to be forgiving to novices but in practice is a really shitty way of doing it.

---

## Hoisting Demo
- hoisting.js

---

## Double Equals

- Always use triple equals (=== and !==)
- Double equals is a big no-no in JS. It's not always symmetric (ie x == y does not mean that y == x.)
- JS tries to do type coercion but does a poor job.
- Examples of strange coercion:

		'' == '0' 			// false
		0 == '' 			// true
		0 == '0'			// true
		false == 'false'	// false
		false == '0'		// true
		false == undefined	// false
		false == null		// false
		null == undefined 	// true
		' \t\r\n ' == 0   	// true

---

## eval() is Evil

- Avoid eval("someFunction()"); It runs the entire JS compiler for a trivial statement and thus is much, much slower. It's never necessary.
- By extension, avoid:

		setTimeout("someFunction()", 1000);
		window.setInterval("otherFunction()", 500);

- These statements can be avoided with a little forethought:

		var someFunction = function(){ ... };
		var otherFunction = function(){ ... };
		setTimeout(someFunction, 1000);
		window.setInterval(otherFunction, 500);

---

## Others

- Avoid with(). It was a good idea with poor execution. Don't ever use it.
- JS has bitwise operators. They're inefficient and thus worthless in JS.
- JS's typeof operator does not differentiate between objects and arrays. Be careful.
- NaN is not equal to itself. Use isNaN() to check it.
- Semicolons are _not_ optional in JS. JS will just put them in for you if you forget. This is almost never problematic, but there are isolated cases where it can be.

---

# How the Glue Somehow Gave the Kid Superpowers
## Why JS is awesome

---

## Functions as First Class Objects

- The mystical source of many of JS's superpowers, functions are treated like objects, meaning they can be passed around in function calls.
- This allows cool things like callbacks, promises, asynchronous processing, immediately-invoked function expressions (IIFE), closures, and other coolness.
- It can also cause problems like the pyramid of doom (as known as callback hell) so be careful.

---

![Pyramid of Doom](../pyramid.png)

---

## Aside: Four Ways to Invoke Methods

- It matters because *this* differs in you function depending on how you called it.
- Function invocation: Call a method out of the global scope. The *this* object will be the global/window object. Example: someArray();
- Method invocation: Call a method that's a function in an object. The *this* object will be the object. Example: someObject.someMethod();

---

## More invocations

- Apply invocation: Call the apply method with the function as a parameter. The *this* object will be the argument provided. Example: someObject.apply('someMethod', ['param1', 'param2']);
- Constructor invocation: The method called when the new operator is given. Should be only function with capital first letter. The *this* object will be the new object. Example: var x = SomeObject();

---

## Prototypical Inheritance

- Different than classical inheritance, or what you think of normal inheritance.
- "Classless"
- Think of variables declared in child objects as "obscuring" their prototype's variables.

---

![Prototype](../Prototype.png)

---

## Cool / Scary Parts of Prototypical Inheritance

- You can modify just about anything. You add methods to the object, array, or whatever else you want. Add a method to object prototype and all objects get it.
- You can even modify the values for NaN and undefined. Don't do it. Bad.

---

## Closures

- A closure is a function inside another function that maintains the local variables of the outer function without making them available for modification.
- The only real way to private visibility in JS.
- Example


		function sayHello(name) {
		  var text = 'Hello ' + name; // local variable
		  var sayAlert = function() { alert(text); }
		  return sayAlert;
		}

		var mikko = sayHello('Mikko');
		mikko(); // Hello Mikko

---

## Closure Demo
- nonclosure.js
- closure.js

---

## IIFE

- Stands for instantly-invoked function expressions.
- Syntax is (function() { // function body })();
- Those extra parenthesis are necessary.
- It's a function that is created then ran instantly.
- One of the ways to solve JS's global scope problem. Because function have scope, it instantly gives you a scope where blocks will not.

---

## Cascade

- Also referred to as chaining.
- Those familiar with jQuery will recognize it.
- It's when you return the object itself when a function finishes so that you may chain like-calls together.
- Example


		getElement('myBoxDiv')
			.move(350, 150)
			.width(100)
			.height(100)
			.color('red')
			.border('10px outset')
			.padding('4px')
			.appendText("Please stand by");

--- 

## Radness not covered

- Currying: Combining functions to make new super functions!
- Memoization: Function-level caching in JS
- Promises: A way to chain callbacks with .then() statements. Very similar to using callbacks but it's more readable and it avoids the pyramid of doom. Has some additional power.

---

## More reading

- *JavaScript: The Good Parts* by Douglas Crockford
- *Effective JavaScript* by David Herman
- *JavaScript Patterns* by Stoyan Stefanov




















