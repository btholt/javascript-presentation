// Take each value a and m

var a = ["a", 24, { foo: "bar" }];

function transform(array) {
  var newArray = [];
  for (var i = 0; i < array.length; i++) {
    newArray.push(function(x, localArray) {
            return function(){
                return localArray[x];
            };
        }(i, array));
  }
  return newArray;
}

var b = transform(a);

for (var i = 0; i < b.length; i++) {
    alert(b[i]());
}