# License
- MIT

# Notes
- Feel free to modify, distribute, and plagarize as you please. I used a lot from _Javascript: The Good Parts_ from Douglas Crockford.
- Made using [mdpress](http://documentup.com/egonschiele/mdpress)
