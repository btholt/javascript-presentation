// Don't worry about the weird function notation for now. It's for scope.
// What value will be alerted?

(function(){
    var smartPeople = ["Morgan", "Mikko", "Noelle", "Brian", "Mike", "Stephanie"];
    var i = 10;
    for (var i = 0; i < smartPeople.length; i++) {
        // do nothing
    }
    
    alert(i);

}());