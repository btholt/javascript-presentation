// Take each value in a and turn it into a function that returns its respective value from b.

var a = ["a", 24, { foo: "bar" }];

function transform(array) {
    var newArray = [];
    for (var i = 0; i < array.length; i++) {
        newArray.push(function() { return array[i]; } );
    }
    i = 1;
    return newArray;
}

var b = transform(a);

for (var i = 0; i < b.length; i++) {
    alert(b[i]());
}